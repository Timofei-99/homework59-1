import './App.css';
import {Component} from "react";
import TodoBuilder from "./Components/TodoBuilder/TodoBuilder";

class App extends Component {
  render() {
    return (
        <div className="App">
           <TodoBuilder/>
        </div>
    );
  }
}

export default App;
