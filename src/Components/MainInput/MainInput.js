import React, {Component} from 'react';

class MainInput extends Component {
    render() {
        return (
           <div style={{width: '90%'}} className='input-group mb-3 mt-3 ms-5'>
               <input onChange={this.props.change} type="text " value={this.props.value} className='form-control'/>
               <button
                   onClick={this.props.add}
                   className='btn btn-primary'
                   type='button'
               >Add
               </button>
           </div>
        );
    }
}

export default MainInput;