import React, {Component} from 'react';

class Todo extends Component {

    shouldComponentUpdate(nextProps) {
        return nextProps.value !== this.props.value;
    };

    render() {
        return (
            <li
                className='list-group-item d-flex justify-content-between p-3 mb-2 border-2'
            >
                <input
                    type="text"
                    value={this.props.value}
                    className='form-text'
                    style={{width: '50%'}}
                    onChange={this.props.create}
                />
                <button
                    onClick={this.props.delete}
                    className='btn btn-danger myBtn'
                >X
                </button>
            </li>
        );
    }
}

export default Todo;