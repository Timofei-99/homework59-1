import React, {Component} from 'react';
import MainInput from "../MainInput/MainInput";
import {nanoid} from 'nanoid';
import Todo from "../Todo/Todo";

class TodoBuilder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todo: [],
            inputText: '',
            id: ''
        };


    }

    forChange = (e) => {
        this.setState({inputText: e.target.value})
    };


    add = () => {
        if  (this.state.inputText !== '') {
            const todo = [...this.state.todo];
            const newList = {
                text: this.state.inputText,
                id: nanoid()
            };
            todo.push(newList);
            this.setState({
                todo,
                inputText:''
            });
        }
    }






    inputChange = (e, i) => {
        const todos = [...this.state.todo];
        const todo = {...todos[i]};
        todo.text = e.target.value;
        todos[i] = todo;

        this.setState({
            ...this.state,
            todo: todos,
            inputText: ''
        })
    }


    dlt = (id) => {
        const todo = [...this.state.todo];
            const index = todo.findIndex(t => t.id === id);
            todo.splice(index, 1);
            this.setState({todo});

    }



    render() {
        return (
            <>
                <MainInput
                    value={this.state.inputText}
                    change={(e) => this.forChange(e)}
                    add={() => this.add()}
                />
                <ul className='list-group'>
                    {this.state.todo.map((p, i,) => (
                        <Todo
                            value={p.text}
                            key={p.id}
                            create={(e) => this.inputChange(e, i)}
                            delete={() => this.dlt( p.id)}
                        />
                    ))}
                </ul>

            </>
        );
    }
}

export default TodoBuilder;